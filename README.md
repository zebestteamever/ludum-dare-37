# README #

The One Room Club was made in 72 hours for the 37th Ludum Dare game jam (December 2016).

## Setup

1. Clone the project or [Download the zip](https://bitbucket.org/mkalamalami/ludum-dare-37/downloads)
2. Open the project with Unity 5.5+
3. Do the D.A.N.C.E.!

## Licensing

All our assets and code were made during the jam. We have yet to choose a license for them, until then all rights are reserved.

![logo](http://eguillot.fr/ludum-dare/ld37/jacket.jpg)