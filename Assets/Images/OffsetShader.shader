﻿public class BackgroundFollow : MonoBehaviour {
	Transform control;
	float current_x_offset = 0;
	public float offset = 0f;
	public float x_pos;
	public float y_pos;
	public float z_pos;

	void Awake() {
		control = GameObject.FindGameObjectWithTag("player").transform;
		Vector3 pos = new Vector3(control.position.x + x_pos, y_pos, z_pos);
		transform.position = pos;
	}

	void Start()
	{
		SetOffset();
	}

	public void SetOffset()
	{
		offset = control.position.x - this.transform.position.x;
	}

	public void ScrollImage(float amount)
	{
		Vector3 pos = transform.position;
		pos.x = control.position.x - this.offset;
		transform.position = pos;

		current_x_offset += amount;
		Vector2 offset = new Vector2(current_x_offset, 0f);
		this.GetComponent<Renderer>().sharedMaterial.SetTextureOffset("_MainTex", offset);
	}
}