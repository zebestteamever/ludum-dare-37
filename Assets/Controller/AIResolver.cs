﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIResolver : MonoBehaviour {
    
    private Animator animator;

    private LevelConfigScriptableObject levelConfig;

    private Dictionary<AIRoute, List<UnitController>> unitsByRoute = new Dictionary<AIRoute, List<UnitController>>();

    private Dictionary<UnitController, int> unitsMovementStep = new Dictionary<UnitController, int>();

    private UIPlayerController playerController;

    public void Start()
    {
        animator = GetComponent<Animator>();
        playerController = GameObject.Find("UIPlayerController").GetComponent<UIPlayerController>();
    }
    
	public void Run(NavigationController navigationController)
    {
        StartCoroutine(RunCoroutine(navigationController));
    }

    internal void SetLevelConfig(LevelConfigScriptableObject levelConfig)
    {
        this.levelConfig = levelConfig;
        unitsByRoute.Clear();
        foreach (AIRoute route in levelConfig.aiRoutes)
        {
            unitsByRoute.Add(route, new List<UnitController>());
        }
    }

    public IEnumerator RunCoroutine(NavigationController navigationController)
    {
        int currentTurn = animator.GetInteger("turn");

        if(currentTurn == 1)
        {
            foreach (NodeId position in levelConfig.obstacles)
            {
                UnitController newUnit = navigationController.Spawn(levelConfig.obstacleUnit, position);
            }
        }

        // Movement
        Dictionary<UnitController, List<Direction>> movements = new Dictionary<UnitController, List<Direction>>();
        foreach (AIRoute route in levelConfig.aiRoutes)
        {
            List<UnitController> units = unitsByRoute[route];
            units.RemoveAll(item => item.isDead());

            foreach (UnitController unit in units)
            {
                int nextStepIndex = (unitsMovementStep[unit] + 1) % route.movementLoop.Count;
                Direction nextStep = route.movementLoop[nextStepIndex];
                if (Direction.RANDOM == nextStep)
                {
                    Array values = Enum.GetValues(typeof(Direction));
                    nextStep = (Direction)values.GetValue(UnityEngine.Random.Range(0, values.Length - 1));
                }
                List<Direction> directions = new List<Direction>();
                directions.Add(nextStep);
                movements.Add(unit, directions);
                unitsMovementStep[unit] = nextStepIndex;
            }
        }
        navigationController.MoveAll(movements);

        // Spawn
        foreach (AIRoute route in levelConfig.aiRoutes)
        {
            List<UnitController> units = unitsByRoute[route];
            if (currentTurn >= route.offset && (currentTurn + route.offset) % route.interval == 0 && (route.maxUnitOnDancefloor == 0 || units.Count < route.maxUnitOnDancefloor))
            {
                UnitController newUnit = navigationController.Spawn(route.units[UnityEngine.Random.Range(0,route.units.Count)], route.spawn);
                if (newUnit != null)
                {
                    unitsMovementStep.Add(newUnit, -1);
                    units.Add(newUnit);
                }
            }
        }

        // Victory/Defeat test
        if (playerController.unitCardDeck.Count() == 0 && navigationController.GetUnits(true).Count == 0)
        {
            animator.SetBool("victory", false);
            animator.SetBool("gameOver", true);
        }
        foreach(UnitController unit in navigationController.GetUnits(true))
        {
            if (unit.node.gridX >= navigationController.map.gridSizeX - 2)
            {
                animator.SetBool("victory", true);
                animator.SetBool("gameOver", true);
                break;
            }
        }

        yield return new WaitForSeconds(Utils.BeatDuration()/2f);
        animator.SetTrigger("aiResolutionOver");
    }
}
