﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerResolutionBehaviour : StateMachineBehaviour {

    private UIPlayerController playerController;
    private NavigationController navigationController;
    private UICardComponent playedCard;
    private PlayerResolver playerResolver;

    public void Awake()
    {
        playerController = GameObject.Find("UIPlayerController").GetComponent<UIPlayerController>();
        navigationController = GameObject.Find("MapGenerator").GetComponent<NavigationController>();
    }

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        playedCard = playerController.playedCard;
        playerResolver = animator.GetComponent<PlayerResolver>();
        playerResolver.Run(playedCard, navigationController);
    }
    
}
