﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuBehaviour : StateMachineBehaviour {

    private GameObject mainMenuCanvas;
    private UIPlayerController uiPlayerController;
    private GameObject gameOverCanvas;
    private GameObject gameplayCanvas;
    private GameObject discoBall;
    private AudioClip menuMusic;
    private AudioSource audioSource;

    public void Awake()
    {
        uiPlayerController = GameObject.Find("UIPlayerController").GetComponent<UIPlayerController>();
        audioSource = GameObject.Find("TurnController").GetComponent<AudioSource>();
        discoBall = GameObject.Find("DiscoBall");
    }

	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        GameObject.Find("TurnController").GetComponent<LevelConfigurationController>().InitBehaviourBindings();
        mainMenuCanvas.SetActive(true);
        gameOverCanvas.SetActive(false);
        gameplayCanvas.SetActive(false);
        uiPlayerController.SetHudVisible(false);
        discoBall.SetActive(false);
        Utils.SwitchMusic(audioSource, menuMusic, animator);
        GameObject.Find("MapGenerator").GetComponent<NavigationController>().GameInit();

        mainMenuCanvas.GetComponent<MainMenuController>().RefreshMenu();
    }

	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        discoBall.SetActive(true);
        mainMenuCanvas.SetActive(false);
    }

    internal void SetCanvas(GameObject mainMenuCanvas, GameObject gameOverCanvas, GameObject gameplayCanvas)
    {
        this.mainMenuCanvas = mainMenuCanvas;
        this.gameOverCanvas = gameOverCanvas;
        this.gameplayCanvas = gameplayCanvas;
    }

    internal void SetAudioClip(AudioClip menuMusic)
    {
        this.menuMusic = menuMusic;
    }
}
