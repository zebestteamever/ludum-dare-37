﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIResolutionBehaviour : StateMachineBehaviour {

    private UIPlayerController playerController;

    private NavigationController navigationController;

    private AIResolver aiResolver = null;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        navigationController = GameObject.Find("MapGenerator").GetComponent<NavigationController>();
        playerController = GameObject.Find("UIPlayerController").GetComponent<UIPlayerController>();
        aiResolver = animator.GetComponent<AIResolver>();
        aiResolver.Run(navigationController);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
    }

}
