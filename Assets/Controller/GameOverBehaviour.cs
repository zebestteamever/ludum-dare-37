﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverBehaviour : StateMachineBehaviour {

    private GameObject gameOverCanvas;
    private GameObject gameplayCanvas;

    private UIPlayerController playerController;
    private AudioSource audioSource;

    public void Awake()
    {
        gameOverCanvas = GameObject.Find("GameOverCanvas");
        playerController = GameObject.Find("UIPlayerController").GetComponent<UIPlayerController>();
        audioSource = GameObject.Find("TurnController").GetComponent<AudioSource>();
    }

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        playerController.SetHudVisible(false);
        gameOverCanvas.GetComponent<GameOverController>().SetLevelConfig(playerController.levelConfig);
        gameOverCanvas.GetComponent<GameOverController>().Configure(animator);
        gameOverCanvas.SetActive(true);
        audioSource.Stop();
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        gameOverCanvas.SetActive(false);
    }

    internal void SetCanvas(GameObject gameOverCanvas, GameObject gameplayCanvas)
    {
        this.gameOverCanvas = gameOverCanvas;
        this.gameplayCanvas = gameplayCanvas;
    }
}
