﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewTurnBehaviour : StateMachineBehaviour {

    private UIPlayerController playerController;

    private NavigationController navigationController;

    private InGameController inGameController;

    private Animator animator;

    public void Awake()
    {
        UIPlayerController.NewTurnOverEvent += OnNewTurnOver;
        playerController = GameObject.Find("UIPlayerController").GetComponent<UIPlayerController>();
        navigationController = GameObject.Find("MapGenerator").GetComponent<NavigationController>();
        inGameController = GameObject.Find("InGameCanvas").GetComponent<InGameController>();
    }

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        this.animator = animator;
        int nextTurn = animator.GetInteger("turn") + 1;
        animator.SetInteger("turn", nextTurn);

        if (nextTurn > 9)
        {
            inGameController.UpdateTurn(nextTurn - 9);
            playerController.NewTurn();
            navigationController.ResetPlayerSpawnNode();
        }
        else
        {
            OnNewTurnOver();
        }
    }

    public void OnNewTurnOver()
    {
        animator.SetTrigger("newTurnOver");
    }
    
}
