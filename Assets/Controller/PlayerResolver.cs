﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerResolver : MonoBehaviour {
    
    private Animator animator;

    private LevelConfigScriptableObject levelConfig;

    public void Start()
    {
        animator = GetComponent<Animator>();
    }
    
	public void Run(UICardComponent card, NavigationController navigationController)
    {
        StartCoroutine(RunCoroutine(card, navigationController));
    }

    internal void SetLevelConfig(LevelConfigScriptableObject levelConfig)
    {
        this.levelConfig = levelConfig;
    }

    public IEnumerator RunCoroutine(UICardComponent card, NavigationController navigationController)
    {
        if (card != null)
        {
            CardSpecScriptableObject cardSpec = card.GetSpec();

            if (cardSpec.unitToSpawn != null)
            {
                navigationController.Spawn(cardSpec.unitToSpawn, navigationController.GetPlayerSpawnNode());
            }
            else
            {
                List<UnitController> units = new List<UnitController>(navigationController.GetUnits(true));

                Dictionary<UnitController, List<Direction>> movements = new Dictionary<UnitController, List<Direction>>();
                foreach (UnitController unit in units)
                {
                    List<Direction> directions = new List<Direction>();
                    for (int i = 0; i < cardSpec.movementAmount; i++)
                    {
                        directions.Add(cardSpec.movementDirection);
                    }
                    movements.Add(unit, directions);
                }
                navigationController.MoveAll(movements);
            }

            yield return new WaitForSeconds(Utils.BeatDuration());
        }
        animator.SetTrigger("playerResolutionOver");
    }
}
