﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActionBehaviour : StateMachineBehaviour {

    private UIPlayerController playerController;

    private Animator animator;

    public void Awake()
    {
        UIPlayerController.PlayerActionOverEvent += OnPlayerActionOver;
    }

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        this.animator = animator;
        playerController = GameObject.Find("UIPlayerController").GetComponent<UIPlayerController>();
        playerController.actionAllowed = true;
        playerController.ActionTimer();
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        playerController.actionAllowed = false;
    }

    public void OnPlayerActionOver()
    {
        animator.SetTrigger("playerActionOver");
    }
    
}
