﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInitBehaviour : StateMachineBehaviour
{

    private NavigationController navigationController;

    private Animator animator;
    
    public void Awake()
    {
        navigationController = GameObject.Find("MapGenerator").GetComponent<NavigationController>();
    }

	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        this.animator = animator;

        animator.GetComponent<LevelConfigurationController>().Run(animator.GetInteger("level"));
        navigationController.GameInit();
        animator.SetBool("gameOver", false);
        animator.SetBool("victory", false);
        animator.SetBool("restart", false);
        animator.SetInteger("turn", 0);
        animator.SetTrigger("gameInitOver");
    }

}
