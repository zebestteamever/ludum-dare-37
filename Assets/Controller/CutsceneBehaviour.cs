﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneBehaviour : StateMachineBehaviour {

    private UIPlayerController playerController;

    private CutsceneController cutsceneController;

    private GameObject gameplayCanvas;

    private AudioSource audioSource;

    private AudioClip menuMusic;
    private AudioClip levelMusic;

    public void Awake()
    {
        playerController = GameObject.Find("UIPlayerController").GetComponent<UIPlayerController>();
        cutsceneController = GameObject.Find("TurnController").GetComponent<CutsceneController>();
        audioSource = GameObject.Find("TurnController").GetComponent<AudioSource>();
    }

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Utils.SwitchMusic(audioSource, menuMusic, animator);
        animator.SetBool("gameOver", false);
        animator.SetBool("victory", false);
        animator.SetBool("restart", false);
        playerController.SetHudVisible(true);
        gameplayCanvas.SetActive(true);
        if (animator.GetBool("restart"))
        {
            cutsceneController.SkipCutsceneAndInitCards(animator, levelMusic);
        }
        else
        {
            cutsceneController.RunCutsceneAndInitCards(animator, levelMusic);
        }
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Utils.SwitchMusic(audioSource, levelMusic, animator);
    }

    internal void SetCanvas(GameObject gameplayCanvas)
    {
        this.gameplayCanvas = gameplayCanvas;
        cutsceneController.SetCanvas(gameplayCanvas);
    }

    internal void SetAudioClips(AudioClip menuMusic, AudioClip levelMusic)
    {
        this.menuMusic = menuMusic;
        this.levelMusic = levelMusic;
    }
    
}
