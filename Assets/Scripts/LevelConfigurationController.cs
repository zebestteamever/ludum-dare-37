﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelConfigurationController : MonoBehaviour
{
    public List<LevelConfigScriptableObject> levels;

    public GameObject gameOverCanvas;

    public GameObject mainMenuCanvas;

    public GameObject gameplayCanvas;

    public AudioClip menuMusic;

    public List<AudioClip> levelsMusics;

    private int currentMusicIndex = -1;
	
    private void Awake()
    {
        SaveStateManager.Load();
        InitGameState();
    }

    public void InitBehaviourBindings()
    { 
        GetComponent<Animator>().GetBehaviour<MainMenuBehaviour>().SetCanvas(mainMenuCanvas, gameOverCanvas, gameplayCanvas);
        GetComponent<Animator>().GetBehaviour<MainMenuBehaviour>().SetAudioClip(menuMusic);
        GetComponent<Animator>().GetBehaviour<GameOverBehaviour>().SetCanvas(gameOverCanvas, gameplayCanvas);
        GetComponent<Animator>().GetBehaviour<CutsceneBehaviour>().SetCanvas(gameplayCanvas);
    }

    public void Run(int levelId)
    {
        int levelIndex = 0;
        if (levels.Count >= levelId)
        {
            levelIndex = levelId - 1;
        }
        else
        {
            Debug.Log("OMG the level " + levelId + " can't be found in the LevelConfigurationController");
        }

        LevelConfigScriptableObject levelConfig = levels[levelId - 1];
        GetComponent<PlayerResolver>().SetLevelConfig(levelConfig);
        GetComponent<AIResolver>().SetLevelConfig(levelConfig);
        GetComponent<TutorialFactoryComponent>().SetLevelConfig(levelConfig);
        GetComponent<CutsceneController>().SetLevelConfig(levelConfig);
        GameObject.Find("UIPlayerController").GetComponent<UIPlayerController>().levelConfig = levelConfig;
        GameObject.Find("MapGenerator").GetComponent<NavigationController>().SetLevelConfig(levelConfig);

        currentMusicIndex = (currentMusicIndex + 1) % levelsMusics.Count;
        AudioClip levelMusic = levelsMusics[currentMusicIndex];
        GetComponent<Animator>().GetBehaviour<CutsceneBehaviour>().SetAudioClips(menuMusic, levelMusic);
    }

    private void InitGameState()
    {
        if (SaveStateManager.gameState == null)
        {
            SaveStateManager.gameState = new GameState();
        }
        
        // Add level save conf even if we have a new version
        for(int i= SaveStateManager.gameState.levels.Count; i < levels.Count; i++)
        {
            SaveStateManager.gameState.levels.Add(new Level(i + 1));
        }
    }



}
