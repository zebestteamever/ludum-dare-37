﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraInitController : MonoBehaviour {

    private float ratio;

    void Start () {
        UpdateCameraSize();
    }
	
	void Update ()
    {
        UpdateCameraSize();
    }

    void UpdateCameraSize()
    {
        float currentRatio = Screen.width * 1f / Screen.height;
        if (currentRatio != ratio)
        {
            ratio = currentRatio;
            Camera camera = GetComponent<Camera>();
            float excessHeight = 16f / 9f - ratio;
            if (excessHeight > 0)
            {
                camera.orthographicSize = 10 + excessHeight * 5;
            }
            else
            {
                camera.orthographicSize = 10;
            }
        }
    }
}
