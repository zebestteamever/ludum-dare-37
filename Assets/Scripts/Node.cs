﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node {

    Map map;
    GameObject caseTile;

    public Vector3 worldPosition;
    public Vector2 size;

    public int gridX;
    public int gridY;

    public int layerOrder;
    public UnitController unit;


    public Node(GameObject caseTile, Map map, Vector3 worldPosition, int gridX, int gridY)
    {
        this.caseTile = caseTile;
        Renderer sprite = caseTile.GetComponent<Renderer>();
        size = new Vector2(sprite.bounds.size.x, sprite.bounds.size.y);
        this.map = map;
        this.worldPosition = worldPosition;
        this.gridX = gridX;
        this.gridY = gridY;
    }

    public void Enter(UnitController _unit)
    {
        unit = _unit;
        unit.node = this;
    }

    public void Leave()
    {
        unit = null;
    }



    public bool hasUnit
    {
        get
        {
            return unit != null;
        }
    }

}
