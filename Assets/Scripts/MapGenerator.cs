﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapGenerator : MonoBehaviour {

    public GameObject tilePrefab;
    public int gridSizeX;
    public int gridSizeY;

    public Vector2 offset;

    Map map;
    public NavigationController navigationController;

    void Awake()
    {
        navigationController = GetComponent<NavigationController>();
    }

    void Start () {
        GenerateMap();
        navigationController.map = map;
    }
	
	public void GenerateMap()
    {
        map = new Map(gridSizeX, gridSizeY);
        float tileWidth = tilePrefab.GetComponent<SpriteRenderer>().bounds.size.x;
        float tileHeight = tilePrefab.GetComponent<SpriteRenderer>().bounds.size.y;

        int maxRectHeight = gridSizeY;
        int maxRectWidth = (gridSizeX - 1) / 2;
        for (int x = 0; x < gridSizeX; x++)
        {
            for (int y =0; y < gridSizeY; y++)
            {
                if(x < gridSizeX - 2 || (y >= 2 && y < gridSizeY - 2))
                {
                    float posX = -(gridSizeX * tileWidth) / 2 + tileWidth / 2 + x * tileWidth + offset.x;
                    float posY = -(gridSizeY * tileHeight) / 2 + tileHeight / 2 + y * tileHeight + offset.y;
                    Vector3 tilePosition = new Vector3(posX, posY, 0);
                    GameObject newTile = Instantiate(tilePrefab, tilePosition, Quaternion.identity) as GameObject;
                    map.CreateNode(newTile, tilePosition, x, y);

                    Animator animator = newTile.GetComponent<Animator>();
                    if (x < gridSizeX - 2)
                    {
                        int xCorr = x;
                        if (xCorr > maxRectWidth)
                        {
                            xCorr -= maxRectWidth;
                        }
                        if (IsOutsideRect(1, maxRectWidth - 2, 1, maxRectHeight - 2, xCorr, y))
                        {
                            animator.SetTrigger("Type1");
                        }
                        else if (IsOutsideRect(2, maxRectWidth - 3, 2, maxRectHeight - 3, xCorr, y))
                        {
                            animator.SetTrigger("Type2");
                        }
                        else
                        {
                            animator.SetTrigger("Type3");
                        }
                    } else
                    {
                        animator.SetTrigger("Type4");
                        animator.SetInteger("Variant", (x + y) % 2);
                    }

                    //animator.SetTrigger("Type" + ((x + y) % 4 + 1));
                }
            }
        }
    }

    bool IsOutsideRect(int x1, int x2, int y1, int y2, int x, int y)
    {
        return x < x1 || x > x2 || y < y1 || y > y2;
    }
}
