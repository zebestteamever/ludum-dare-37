﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName="Unit",menuName ="Unit", order =2)]
[Serializable]
public class Unit : ScriptableObject {

    public string unitName;
    public Sprite sprite;
    public bool playable;
    public bool killable = true;
    public bool obstacle = false;

}
