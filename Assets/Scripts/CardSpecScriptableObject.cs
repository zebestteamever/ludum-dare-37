﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Card", menuName = "Card", order = 1)]
public class CardSpecScriptableObject : ScriptableObject {

    public Sprite sprite;

    public Sprite label;

    public Direction movementDirection;

    public int movementAmount = 0;

    public Unit unitToSpawn;

    public int frequencePonderation;

}
