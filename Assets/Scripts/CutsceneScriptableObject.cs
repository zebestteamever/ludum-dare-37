﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Cutscene", menuName = "Cutscene", order = 4)]
public class CutsceneScriptableObject : ScriptableObject
{

    public List<CutscenePicture> pictures;

    [System.Serializable]
    public class CutscenePicture
    {
        public Sprite sprite;
        public Vector2 position;
        public Vector2 anchor;
        public float delay;
        public float duration;
    }

}
