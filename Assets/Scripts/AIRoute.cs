﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AIRoute
{

    public NodeId spawn;

    public List<Unit> units;

    public List<Direction> movementLoop;

    // Turn count between spawn
    public int interval = 2;

    // Offset after first turn before starting to spawn
    public int offset;

    public int maxUnitOnDancefloor;

}