﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FouleComponent : MonoBehaviour {

    private AudioSource audioSource;

    private const float BASE_FACTOR = 0.5f;
    private int lastTurn = 0;
    private float speedFactor = BASE_FACTOR;
    public float speed = 0.05f;
    private float offsetX;

    void Awake()
    {
        audioSource = GameObject.Find("TurnController").GetComponent<AudioSource>();
    }

    void Start () {
        offsetX = Random.Range(0f, 1f);
    }
	
	void Update () {
        int currentTurn = Utils.GetCurrentBeat(audioSource);
        if (lastTurn == currentTurn || lastTurn == currentTurn - 1)
        {
            speedFactor *= 1.03f;
        }
        else
        {
            speedFactor = BASE_FACTOR;
            lastTurn = currentTurn;
        }

        GetComponent<Renderer>().material.mainTextureOffset += new Vector2(Time.deltaTime * speed * speedFactor, 0);
    }
}
