﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level", menuName = "Level", order = 3)]
public class LevelConfigScriptableObject : ScriptableObject {

    public List<AIRoute> aiRoutes;

    public List<NodeId> obstacles;

    public Unit obstacleUnit;

    public List<CardSpecScriptableObject> allowedCards;

    public List<TutorialEntry> tutorials;

    public CutsceneScriptableObject cutscene;

    public int playableUnitCount;

    public List<NodeId> playerSpawnPoints;

    public Unit firstUnit;

    public int nbTurnBonusObjective;

    [System.Serializable]
    public class TutorialEntry {
        public string id;
        public string message;
    }

}
