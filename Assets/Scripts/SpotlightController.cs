﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpotlightController : MonoBehaviour {

    public int xRange;
    public int yRange;

    public float speed = 1f;

    private Vector3 startPosition;

    float resetTime = 0f;
    float nextResetTime = 0f;

    bool lightActive = true; 
    int moveStyle;

    Transform lightTransform;
    Light lightComponent;

    // Use this for initialization
    void Awake () {
        lightTransform = GetComponent<Transform>();
        lightComponent = GetComponent<Light>();
        startPosition = lightTransform.position;
        moveStyle = Random.Range(1, 5);
    }
	
	// Update is called once per frame
	void Update () {
        if(!lightActive && Time.time - resetTime > 1f)
        {
            toggleActive();
        }

        float step = speed * Time.deltaTime;
        // transform.position = Vector3.MoveTowards(transform.position, target.position, step);
        // Vector3 diff = startPosition - transform.position;
        if (Time.time - resetTime > nextResetTime)
        {
            nextResetTime = Random.Range(5.0f, 10.0f);
            moveStyle = Random.Range(1, 5);
            resetTime = Time.time;
            toggleActive();
        }
        MoveLight();
    }

    void toggleActive()
    {
        lightActive = !lightActive;
        if (lightActive)
        {
            lightComponent.intensity = 8;
        } else
        {
            lightComponent.intensity = 0;
        }

    }

    void MoveLight()
    {
        float correctedTime = Time.time * speed;
        if (moveStyle == 1)
        {
            transform.position = startPosition + new Vector3(Mathf.Sin(correctedTime) * xRange, Mathf.Sin(correctedTime) * yRange, 0.0f);
        } else if (moveStyle == 2)
        {
            transform.position = startPosition + new Vector3(Mathf.Sin(correctedTime) * xRange, Mathf.Cos(correctedTime) * yRange, 0.0f);
        }
        else if (moveStyle == 3)
        {
            transform.position = startPosition + new Vector3(Mathf.Cos(correctedTime) * xRange, Mathf.Cos(correctedTime) * yRange, 0.0f);
        }
        else if (moveStyle == 4)
        {
            transform.position = startPosition + new Vector3(Mathf.Cos(correctedTime) * xRange, Mathf.Sin(correctedTime) * yRange, 0.0f);
        }
    }
}
