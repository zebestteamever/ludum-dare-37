﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Level  {

    public int num;

    public bool completed = false;
    public bool objectifCompleted = false;
    public bool hardCoreCompleted = false;

    public Level(int num)
    {
        this.num = num;
    }
}
