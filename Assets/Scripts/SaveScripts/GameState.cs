﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameState {

    public List<Level> levels = new List<Level>();

    public void CompleteLevel(int level)
    {
        levels[level - 1].completed = true;
    }

    public void CompleteObjectifLevel(int level)
    {
        levels[level - 1].objectifCompleted = true;
    }

    public void CompleteHardCoreLevel(int level)
    {
        levels[level - 1].hardCoreCompleted = true;
    }
}
