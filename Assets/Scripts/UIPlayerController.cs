﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPlayerController : MonoBehaviour {

    public const int handSize = 5;

    public GameObject mouvementCardTemplate;
    public GameObject unitCardTemplate;

    public UIDeckComponent handDeck;
    public UIDeckComponent activeCardDeck;
    public UIDeckComponent unitCardDeck;
    public UIDeckComponent graveyardDeck;

    public GameObject selectedCardIndicator;

    public LevelConfigScriptableObject levelConfig;

    public UICardComponent selectedCard;
    public UICardComponent playedCard;
    public bool actionAllowed = false;

    public delegate void NewTurnOver();
    public static event NewTurnOver NewTurnOverEvent;

    public delegate void PlayerActionOver();
    public static event PlayerActionOver PlayerActionOverEvent;

    private NavigationController navigationController;
    private AudioSource audioSource;

    private int cardIndex = 0;
    private List<CardSpecScriptableObject> allowedCards = new List<CardSpecScriptableObject>();
    private List<CardSpecScriptableObject> allowedUnitCards = new List<CardSpecScriptableObject>();
    private int previousBeat = -1;

    void Awake()
    {
        navigationController = GameObject.Find("MapGenerator").GetComponent<NavigationController>();
        audioSource = GameObject.Find("TurnController").GetComponent<AudioSource>();
    }

    void Start ()
    {
        mouvementCardTemplate.SetActive(false);
        unitCardTemplate.SetActive(false);
        UICardComponent.CardClickEvent += OnCardClick;
    }

    void Update()
    {
        if (Input.GetKey("escape"))
            Application.Quit();
    }

    public void SetHudVisible(bool visible)
    {
        handDeck.gameObject.SetActive(visible);
        unitCardDeck.gameObject.SetActive(visible);
    }

    internal void ActionTimer()
    {
        StartCoroutine(ActionTimerCoroutine());
    }

    private IEnumerator ActionTimerCoroutine()
    {
        playedCard = null;
        // AutoSelectCard();
        if (previousBeat == -1)
        {
            previousBeat = Utils.GetCurrentBeat(audioSource);
            if (previousBeat % 2 == 1) previousBeat--; // kicks > snares
        }
        while (!Utils.IsNBeatsAfter(previousBeat, 4, audioSource))
        {
            yield return new WaitForSeconds(0.1f);
        }
        previousBeat = Utils.GetCurrentBeat(audioSource);
        yield return PlayCardCoroutine(selectedCard);
    }
    
    // UNUSED
    public void AutoSelectCard()
    {
        if (selectedCard == null)
        {
            bool livingDwarves = navigationController.GetUnits(true).Count > 0;
            if (livingDwarves)
            {
                SelectCard(handDeck.cards[0]);
            }
            else if (unitCardDeck.Count() > 0)
            {
                SelectCard(unitCardDeck.cards[0]);
            }
        }
    }

    private void InitAllowedCards()
    {
        allowedCards = new List<CardSpecScriptableObject>();
        foreach(CardSpecScriptableObject card in levelConfig.allowedCards)
        {
            if (card.unitToSpawn == null)
            {
                for (int i = 0; i < card.frequencePonderation; i++)
                {
                    allowedCards.Add(card);
                }
            }
        }

        allowedUnitCards = new List<CardSpecScriptableObject>();
        foreach (CardSpecScriptableObject card in levelConfig.allowedCards)
        {
            if (card.unitToSpawn != null)
            {
                for (int i = 0; i < card.frequencePonderation; i++)
                {
                    allowedUnitCards.Add(card);
                }
            }
        }
    }

    public void NewTurn()
    {
        StartCoroutine(NewTurnCoroutine());
    }

    public void OnCardClick(UICardComponent card)
    {
        // Check if player has units
        if (card.currentDeck == handDeck && navigationController.GetUnits(true).Count == 0)
        {
            Debug.Log("Can't use a movement card!");
        }
        else
        {
            SelectCard(card);
        }
    }

    public void SelectCard(UICardComponent card)
    {
        if (card == null)
        {
            selectedCard.UnSelect();
            selectedCard = null;
        }
        else if (card.currentDeck == handDeck || card.currentDeck == unitCardDeck)
        {
            // selectedCardIndicator.GetComponent<TargetPositionController>().targetPosition = card.gameObject.transform.position;
            if(selectedCard != null)
            {
                selectedCard.UnSelect();
            }
			
            selectedCard = card;
			selectedCard.Select();
        }
    }

    public IEnumerator InitCardsCoroutine()
    {
        InitAllowedCards();

        // selectedCardIndicator.GetComponent<TargetPositionController>().targetPosition = new Vector3(0, -15, selectedCardIndicator.transform.position.z);

        previousBeat = -1;
        actionAllowed = false;

        unitCardDeck.DestroyAllCards();
        handDeck.DestroyAllCards();
        activeCardDeck.DestroyAllCards();

        for (int i = 0; i < levelConfig.playableUnitCount; i++)
        {
            CreateCard(unitCardDeck, allowedUnitCards);
            yield return new WaitForSeconds(.1f);
        }

        for (int i = 0; i < handSize; i++)
        {
            CreateCard(handDeck, allowedCards);
            yield return new WaitForSeconds(.2f);
        }
    }

    private IEnumerator NewTurnCoroutine()
    {
        while (handDeck.Count() < handSize)
        {
            CreateCard(handDeck, allowedCards);
            yield return new WaitForSeconds(.2f);
        }
        NewTurnOverEvent();
    }

    private IEnumerator PlayCardCoroutine(UICardComponent card)
    {
        if (actionAllowed && card != null && (card.currentDeck == handDeck || card.currentDeck == unitCardDeck) && activeCardDeck.Count() == 0)
        {
            playedCard = card;
            SelectCard(null);
            if (card.currentDeck == handDeck)
            {
                CreateCard(handDeck, allowedCards, card.positionInDeck);
            }
            else
            {
                StartCoroutine(ResetHand(playedCard));
            }
            activeCardDeck.AddCard(card);
            activeCardDeck.RefreshLayout();
            handDeck.RefreshLayout();
            unitCardDeck.RefreshLayout();
            PlayerActionOverEvent();
            yield return new WaitForSeconds(Utils.BeatDuration() * 3);
            graveyardDeck.AddCard(card);
            activeCardDeck.RefreshLayout();
            graveyardDeck.RefreshLayout();
            yield return new WaitForSeconds(1.0f);
            graveyardDeck.DestroyAllCards();
        }
        else
        {
            PlayerActionOverEvent();
        }
    }

    private IEnumerator ResetHand(UICardComponent playedCard)
    {
        for (int i = 0; i < handSize; i++)
        {
            if (handDeck.cards[0] != playedCard)
            {
                graveyardDeck.AddCard(handDeck.cards[0]);
            }
            CreateCard(handDeck, allowedCards);
            yield return new WaitForSeconds(.1f);
        }
        handDeck.RefreshLayout();
        graveyardDeck.RefreshLayout();
        graveyardDeck.DestroyAllCards();
    }

    public bool AlreadyHaveThatCardTooMuch(CardSpecScriptableObject potentiallyShitCard)
    {
        int count = 0;
        foreach (UICardComponent card in handDeck.cards)
        {
            if (card.GetSpec() == potentiallyShitCard)
            {
                count++;
            }
        }

        float probaCard = (potentiallyShitCard.frequencePonderation * handSize) / (float) allowedCards.Count;

        if (count !=0 && count >= probaCard )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // Creates a new card and puts it in the hand deck
    private void CreateCard(UIDeckComponent deck, List<CardSpecScriptableObject> allowedCards)
    {
        CreateCard(deck, allowedCards, -1);
    }

    private void CreateCard(UIDeckComponent deck, List<CardSpecScriptableObject> allowedCards, int positionInDeck)
    {
        if (allowedCards.Count > 0)
        {
            CardSpecScriptableObject randomCardSpec = allowedCards[UnityEngine.Random.Range(0, allowedCards.Count)];
            if(allowedCards.Count > 1)
            {
                while (AlreadyHaveThatCardTooMuch(randomCardSpec) && UnityEngine.Random.Range(0, 3) < 2)
                {
                    // reroll shit cards
                    randomCardSpec = allowedCards[UnityEngine.Random.Range(0, allowedCards.Count)];
                }
            }

            GameObject newCard;
            if(randomCardSpec.unitToSpawn != null)
            {
                newCard = (GameObject)Instantiate(unitCardTemplate, deck.transform.position + new Vector3(0, 4, 0), Quaternion.Euler(0, 180, 0));
            }
            else
            {
                newCard = (GameObject)Instantiate(mouvementCardTemplate, deck.transform.position + new Vector3(0, 4, 0), Quaternion.Euler(0, 180, 0));
            }
                       
            newCard.SetActive(true);
            newCard.name = "Card " + cardIndex++;

            UICardComponent newCardComponent = newCard.GetComponent<UICardComponent>();
        
            deck.AddCard(newCardComponent, positionInDeck);
            deck.RefreshLayout();

            // move up animation
            newCardComponent.JumpToTargetPosition();
            newCardComponent.transform.Translate(0, -2, 0);

            newCardComponent.ConfigureWithSpec(randomCardSpec);
        }
    }
}
