﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetPositionController : MonoBehaviour {

    private static float MOVEMENT_SPEED = 70f;

    public Vector3 targetPosition = Vector3.zero;
    public Vector3 targetScale = Vector3.zero;

    private Vector3 initialPosition;
    private Vector3 initialTarget;

    private Vector3 positionVelocity;
    private Vector3 scaleVelocity;

    void Awake()
    {        
        if (targetPosition == Vector3.zero)
        {
            targetPosition = transform.position;
        }
        if (targetScale == Vector3.zero)
        {
            targetScale = transform.localScale;
        }
        if (initialPosition == Vector3.zero)
        {
            initialPosition = transform.position;
            initialTarget = targetPosition;
        }
    }

    void Update ()
    {
        
        Vector3 intermediatePosition = Vector3.SmoothDamp(transform.position, targetPosition, ref positionVelocity, 0.1f, MOVEMENT_SPEED);
        intermediatePosition.z = targetPosition.z;
        transform.position = intermediatePosition;
        

       // transform.position = Vector3.MoveTowards(transform.position, targetPosition, Time.deltaTime * MOVEMENT_SPEED);

        transform.localScale = Vector3.SmoothDamp(transform.localScale, targetScale, ref scaleVelocity, 0.1f, MOVEMENT_SPEED);
    }

    public void ResetPosition()
    {
        transform.position = initialPosition;
        targetPosition = initialTarget;
    }
}
