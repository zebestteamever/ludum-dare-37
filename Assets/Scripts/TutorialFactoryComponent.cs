﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialFactoryComponent : MonoBehaviour {

    /**
        TutorialFactoryComponent tutorialFactory = GameObject.Find("TurnController").GetComponent<TutorialFactoryComponent>();
        tutorialFactory.CloseTutorial("test");
        tutorialFactory.StartTutorialOnce("test", new Vector2(200,200), new Vector2(0.1f, 0.1f), 4);
     */

    private static List<string> hookedTutorials = new List<string>();

    private static Dictionary<string, GameObject> activeTutorials = new Dictionary<string, GameObject>();

    public GameObject tutorialPopupPrefab;

    private LevelConfigScriptableObject levelConfig;

    public void SetLevelConfig(LevelConfigScriptableObject levelConfig)
    {
        this.levelConfig = levelConfig;
    }

    /**
     * Lance un tutoriel
     */
    public void StartTutorialOnce(string id, Vector2 position, Vector2 anchor, float maxDuration)
    {
        lock (hookedTutorials)
        {
            if (!hookedTutorials.Contains(id))
            {
                foreach (LevelConfigScriptableObject.TutorialEntry tutorialEntry in levelConfig.tutorials)
                {
                    if (tutorialEntry.id == id)
                    {
                        StartCoroutine(CreateTutorial(id, tutorialEntry.message, position, anchor, maxDuration));
                    }
                }
                hookedTutorials.Add(id);
            }
        }
    }

    public void CloseTutorial(string id)
    {
        lock (hookedTutorials)
        {
            if (activeTutorials.ContainsKey(id))
            {
                GameObject popup = activeTutorials[id];
                activeTutorials.Remove(id);
            //    Object.Destroy(popup);
            }
        }
    }

    public void Reset()
    {
        lock (hookedTutorials)
        {
            hookedTutorials.Clear();
        }
    }

    private IEnumerator CreateTutorial(string id, string message, Vector2 position, Vector2 anchor, float maxDuration)
    {
        GameObject canvas = GameObject.Find("MainMenuCanvas");
        GameObject popup = (GameObject) Object.Instantiate(tutorialPopupPrefab, canvas.transform);
        RectTransform popupRectTransform = popup.GetComponent<RectTransform>();
        popupRectTransform.anchorMin = anchor;
        popupRectTransform.anchorMax = anchor;
        popupRectTransform.position = new Vector3(position.x, position.y, 10);
        popupRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 300);
        popupRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 150);
        popupRectTransform.FindChild("Text").GetComponent<Text>().text = message;
        lock (hookedTutorials)
        {
            activeTutorials.Add(id, popup);
        }
        yield return new WaitForSeconds(maxDuration);
        lock (hookedTutorials)
        {
            activeTutorials.Remove(id);
        }
     //   Object.Destroy(popup);
    }

}
