﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneController : MonoBehaviour {

    private LevelConfigScriptableObject levelConfig;

    private UIPlayerController playerController;
    private AudioSource audioSource;
    private GameObject cutsceneRoot;

    private GameObject gameplayCanvas;

    public void Awake()
    {
        playerController = GameObject.Find("UIPlayerController").GetComponent<UIPlayerController>();
        audioSource = GameObject.Find("TurnController").GetComponent<AudioSource>();
        cutsceneRoot = GameObject.Find("Cutscene");
        cutsceneRoot.SetActive(false);
    }

    public void SetLevelConfig(LevelConfigScriptableObject levelConfig)
    {
        this.levelConfig = levelConfig;
    }

    public void RunCutsceneAndInitCards(Animator animator, AudioClip music)
    {
        StartCoroutine(RunCutsceneCoroutine(animator, music));
    }

    public void SkipCutsceneAndInitCards(Animator animator, AudioClip music)
    {
        StartCoroutine(InitCardsCoroutine(animator, music));
    }

    private IEnumerator RunCutsceneCoroutine(Animator animator, AudioClip music)
    {
        if (levelConfig.cutscene != null)
        {
           TargetPositionController[] ts = cutsceneRoot.transform.GetComponentsInChildren<TargetPositionController>();
            foreach (TargetPositionController t in ts)
            {
                t.gameObject.SetActive(false);
            }
            cutsceneRoot.SetActive(true);
            yield return new WaitForSeconds(.5f);
            foreach (TargetPositionController t in ts)
            {
                t.gameObject.SetActive(true);
                t.transform.position += new Vector3(20, 0, 0);
                yield return new WaitForSeconds(2f);
            }
            yield return new WaitForSeconds(1f);
            foreach (TargetPositionController t in ts)
            {
                t.targetPosition = t.transform.position - new Vector3(50, 0, 0);
            }
            yield return new WaitForSeconds(1f);
            cutsceneRoot.SetActive(false);
            foreach (TargetPositionController t in ts)
            {
                t.ResetPosition();
            }

        }
        
        yield return InitCardsCoroutine(animator, music);
    }

    private IEnumerator InitCardsCoroutine(Animator animator, AudioClip music)
    {
        Utils.SwitchMusic(audioSource, music, animator);
        yield return playerController.InitCardsCoroutine();

        int level = animator.GetInteger("level");
        if (level == 1)
        {
            ShowTuto1();
        }
        else
        {
            animator.SetTrigger("cutsceneOver");
        }
    }

    internal void SetCanvas(GameObject gameplayCanvas)
    {
        this.gameplayCanvas = gameplayCanvas;
    }

    void ShowTuto1()
    {
        InGameController inGameController = gameplayCanvas.GetComponent<InGameController>();
        inGameController.tuto1.SetActive(true);
    }
}
