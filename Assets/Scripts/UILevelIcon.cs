﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILevelIcon : MonoBehaviour {

    Image beerCompleteImage;
    Image beerObjectifCompleteImage;
    Image beerHardCoreCompleteImage;
    Button startLevelButton; 
    Text levelText;

    Level level;

    // Use this for initialization
    void Awake () {
        Transform scorePanel = transform.FindChild("ScorePanel").gameObject.GetComponent<Transform>();
        beerCompleteImage = scorePanel.FindChild("BeerComplete").gameObject.GetComponent<Image>();
        beerObjectifCompleteImage = scorePanel.FindChild("BeerObjectifComplete").gameObject.GetComponent<Image>();
        beerHardCoreCompleteImage = scorePanel.FindChild("BeerHardCoreComplete").gameObject.GetComponent<Image>();
        startLevelButton = transform.FindChild("LevelButton").GetComponent<Button>();
        levelText = startLevelButton.transform.FindChild("Text").GetComponent<Text>();
    }
	
	// Update is called once per frame
	public void Configure (Level level) {
        this.level = level;

        levelText.text = level.num.ToString();
        beerCompleteImage.color = new Color(1, 1, 1, level.completed ? 1 : 0.2f);
        beerObjectifCompleteImage.color = new Color(1, 1, 1, level.objectifCompleted ? 1 : 0);
        beerHardCoreCompleteImage.color = new Color(1, 1, 1, level.hardCoreCompleted ? 1 : 0);
    }

    public Button GetStartLevelButton()
    {
        return startLevelButton;
    }

    internal void Refresh()
    {
        Configure(level);
    }
}
