﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum Direction {

    UP, LEFT, RIGHT, DOWN, RANDOM
        
}
