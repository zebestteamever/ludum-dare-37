﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour {

    private Animator animator;
    Transform levelPanel;

    public GameObject levelIconPrefab;

    List<UILevelIcon> icons = new List<UILevelIcon>();

    void Start ()
    {
        animator = GameObject.Find("TurnController").GetComponent<Animator>();

        levelPanel = transform.FindChild("Levels").gameObject.GetComponent<Transform>();
        CreateMenuLevelItems();
    }
	

    public void StartLevel(int level)
    {
        animator.SetInteger("level", level);
        animator.SetTrigger("mainMenuOver");
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void CreateMenuLevelItems()
    {
        int i = 0;
        float offset = (SaveStateManager.gameState.levels.Count - 1) * 100 + 70;
        foreach (Level level in SaveStateManager.gameState.levels)
        {
            GameObject levelBtn = (GameObject)Instantiate(levelIconPrefab);
            levelBtn.transform.SetParent(levelPanel.transform);
            levelBtn.transform.localScale = Vector3.one;
            levelBtn.GetComponent<RectTransform>().localPosition = new Vector3(-offset + i * 250, 0, 0);
            UILevelIcon icon = levelBtn.GetComponent<UILevelIcon>();
            icon.Configure(level);
            icon.GetStartLevelButton().onClick.AddListener(() => { StartLevel(level.num); });
            icons.Add(icon);
            i++;
        }
    }

    internal void RefreshMenu()
    {
        foreach(UILevelIcon icon in icons)
        {
            icon.Refresh();
        }
    }
}
