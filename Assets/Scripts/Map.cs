﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Map {

    public float nodeRadius;

    Node[,] grid;

    public readonly int gridSizeX;
    public readonly int gridSizeY;

    public Map(int gridX, int gridY)
    {
        this.gridSizeX = gridX;
        this.gridSizeY = gridY;
        this.grid = new Node[gridX, gridY];
    }

    public int MaxSize
    {
        get
        {
            return gridSizeX * gridSizeY;
        }
    }


    public void CreateNode(GameObject caseTile, Vector2 worldPosition, int posX, int posY)
    {
        this.grid[posX, posY] = new Node(caseTile, this, worldPosition, posX, posY);
        this.grid[posX, posY].layerOrder = gridSizeY - posY;
    }

    public Node GetNode(int x, int y)
    {
        if(x >= 0 && y >= 0 && x < gridSizeX && y < gridSizeY)
        {
            return grid[x, y];
        }
        else
        {
            return null;
        }
        
    }

    public List<Node> GetNeighbours(Node node)
    {
        List<Node> neighbours = new List<Node>();
        for(int x = -1; x <= 1; x++)
        {
            int currentGridX = node.gridX + x;
            if (currentGridX >= 0 && currentGridX < gridSizeX)
            {
                for (int y = -1; y <= 1; y++)
                {
                    int currentGridY = node.gridY + y;
                    if (currentGridY >= 0 && currentGridY < gridSizeY)
                    {
                        neighbours.Add(grid[currentGridX, currentGridY]);
                    }
                }
            }  
        }
        return neighbours;
    }
}
