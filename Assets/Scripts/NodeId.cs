﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NodeId {

    public int i;

    public int j;

    public NodeId()
    {

    }

    public NodeId(int i, int j)
    {
        this.i = i;
        this.j = j;
    }

}
