﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitController : MonoBehaviour {

    private Rigidbody2D rBody;
    private SpriteRenderer spriteRenderer;

    private Vector2 targetPosition;
    private bool moveToward = false;
    private bool dead = false;

    public Unit unit;
    public Node node;

    // Use this for initialization
    void Awake () {
        rBody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update () {
		if(moveToward)
        {
            Vector2 deltaMouvement = (targetPosition - rBody.position) * Time.deltaTime * 5;
            rBody.MovePosition(rBody.position + deltaMouvement);
            if (deltaMouvement == Vector2.zero)
            {
                moveToward = false;
            }
            if (dead)
            {
                transform.localScale = transform.localScale * 0.99f;
                rBody.MoveRotation(rBody.rotation + Time.deltaTime * 10 * (unit.playable ? -1 : 1));
                int my = (node.gridY < 3) ? 1 : -1;
                rBody.MovePosition(rBody.position + Time.deltaTime * new Vector2(5f * (unit.playable ? -1 : 1), -10 * my));
            }
        } else if (dead)
        {
            Destroy(gameObject);
        }
	}

    public void MoveTo(Vector3 position, int layerOrder)
    {
        if (!dead)
        {
            targetPosition = new Vector2(position.x, position.y);
            spriteRenderer.sortingOrder = layerOrder;
            moveToward = true;
        }
    }

    public int sortingOrder
    {
        set {
            spriteRenderer.sortingOrder = value;
        }
    }

    public void RunAnimation(string animName)
    {
        Debug.Log("Run anim " + animName);
    }

    public void Kill()
    {
        dead = true;
    }

    public bool isDead()
    {
        return dead;
    }
}
