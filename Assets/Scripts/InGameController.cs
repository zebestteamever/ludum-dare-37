﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameController : MonoBehaviour {

    public Animator animator;
    public GameObject tuto1;
    public GameObject tuto2;
    public Text turnTxt;

    public void Awake()
    {
        animator = GameObject.Find("TurnController").GetComponent<Animator>();
        tuto1 = GameObject.Find("Tuto1");
        tuto2 = GameObject.Find("Tuto2");
        turnTxt = GameObject.Find("Turn").GetComponent<Text>();

        tuto1.SetActive(false);
        tuto2.SetActive(false);
    }

    private void Start()
    {
        tuto1.GetComponent<Button>().onClick.AddListener(() => { tuto1.SetActive(false); tuto2.SetActive(true); });
        tuto2.GetComponent<Button>().onClick.AddListener(() => { tuto2.SetActive(false); animator.SetTrigger("cutsceneOver"); });
    }

    public void BackToMenu()
    {
        animator.SetTrigger("backToMenu");
    }

    internal void UpdateTurn(int nextTurn)
    {
        turnTxt.text = nextTurn.ToString();
    }
}
