﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiscoBallComponent : MonoBehaviour {

    private AudioSource audioSource;

    private const float BASE_SPEED = 10;

    private int lastTurn = 0;
    private float speed = 1;

    void Awake()
    {
        audioSource = GameObject.Find("TurnController").GetComponent<AudioSource>();
    }

	void Update () {
        int currentTurn = Utils.GetCurrentBeat(audioSource);
        if (lastTurn == currentTurn || lastTurn == currentTurn - 1)
        {
            speed *= 1.05f;
        }
        else
        {
            speed = BASE_SPEED;
            lastTurn = currentTurn;
        }
        transform.Rotate(new Vector3(0,0,speed * Time.deltaTime));

	}
}
