﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class UICardComponent : MonoBehaviour
{

    private static float MOVEMENT_SPEED = 50f;
    private static float FLIP_SPEED = .05f;
    private static float FADEIN_SPEED = .3f;
    private static Quaternion FRONT_VISIBLE = Quaternion.Euler(0, 180, 0);
    private static Quaternion FRONT_HIDDEN = Quaternion.Euler(0, 0, 0);

    public bool frontVisible = true;
    public Vector3 targetPosition = Vector3.zero;
    public Vector3 targetScale = Vector3.zero;
    public UIDeckComponent currentDeck = null;
    private CardSpecScriptableObject spec;

    private SpriteRenderer label;
    private SpriteRenderer art;
    private SpriteRenderer back;
    private SpriteRenderer selected;

    private float alphaVelocity;
    private Vector3 positionVelocity;
    private Vector3 scaleVelocity;

    public delegate void CardClick(UICardComponent card);
    public static event CardClick CardClickEvent;

    public int positionInDeck = -1;

    void Awake () {
        if (targetPosition == Vector3.zero)
        {
            targetPosition = transform.position;
        }
        if (targetScale == Vector3.zero)
        {
            targetScale = transform.localScale;
        }
        art = transform.FindChild("Art").gameObject.GetComponent<SpriteRenderer>();
        back = transform.FindChild("Back").gameObject.GetComponent<SpriteRenderer>();
        label = transform.FindChild("Label").gameObject.GetComponent<SpriteRenderer>();
        selected = transform.FindChild("Selected").gameObject.GetComponent<SpriteRenderer>();
        // selected.color = new Color(1, 1, 1, 0);

        foreach (SpriteRenderer sprite in getSprites())
        {
            sprite.color = new Color(1, 1, 1, 0);
        }
    }

    private void Start()
    {
        selected.enabled = false;
    }

    public void ConfigureWithSpec(CardSpecScriptableObject spec)
    {
        this.spec = spec;
        art.sprite = spec.sprite;
        label.sprite = spec.label;
    }

    public CardSpecScriptableObject GetSpec()
    {
        return spec;
    }

    public void OnMouseDown()
    {
        CardClickEvent(this);
    }

    public void JumpToTargetPosition()
    {
        transform.position = targetPosition;
    }

    public void SetImage(string id)
    {
        art.sprite = Resources.Load(id, typeof(Sprite)) as Sprite;
    }

    void Update() {
        Vector3 intermediatePosition = Vector3.SmoothDamp(transform.position, targetPosition, ref positionVelocity, 0.1f, MOVEMENT_SPEED);
        intermediatePosition.z = targetPosition.z;
        transform.position = intermediatePosition;

        transform.localScale = Vector3.SmoothDamp(transform.localScale, targetScale, ref scaleVelocity, 0.1f, MOVEMENT_SPEED);

        Quaternion targetRotation = frontVisible ? FRONT_VISIBLE : FRONT_HIDDEN;
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, FLIP_SPEED);

        if (this.alpha < 1)
        {
            this.alpha = Mathf.SmoothDamp(this.alpha, 1, ref alphaVelocity, FADEIN_SPEED);
        }
    }

    private SpriteRenderer[] getSprites()
    {
        return transform.GetComponentsInChildren<SpriteRenderer>();
    }

    float alpha
    {
        get
        {
            return back.color.a;
        }
        set
        {
            float newAlpha;
            foreach (SpriteRenderer sprite in getSprites())
            {
                newAlpha = (value > .9f) ? 1 : value;
                if (sprite != back && !frontVisible) {
                    newAlpha = (newAlpha == 1) ? 1 : 0;
                }
                sprite.color = new Color(1, 1, 1, newAlpha);
            }
        }
    }

    public void Select()
    {
        selected.enabled = true;
    }

    public void UnSelect()
    {
        selected.enabled = false;
    }
}
