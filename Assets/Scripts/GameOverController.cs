﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverController : MonoBehaviour
{

    public Animator animator;
    public Text text;
    public Text turnsText;
    public Text bonusText;
    public GameObject victoryChara;
    public GameObject defeatChara;

    private LevelConfigScriptableObject levelConfig;

    public void Awake()
    {
        text = transform.FindChild("Panel").FindChild("Text").GetComponent<Text>();
        turnsText = transform.FindChild("Panel").FindChild("TurnsText").GetComponent<Text>();
        bonusText = transform.FindChild("Panel").FindChild("BonusText").GetComponent<Text>();
    }

    public void Continue(bool restart)
    {
        animator.SetBool("restart", restart);
        animator.SetTrigger("gameOverOver");
    }

    internal void Configure(Animator animator)
    {
        this.animator = animator;
        if (animator.GetBool("victory"))
        {
            victoryChara.SetActive(true);
            defeatChara.SetActive(false);
            text.text = "Victory";
            int nbTurn = animator.GetInteger("turn") - 9;
            turnsText.text = "VIP Room reached in " + nbTurn + " turns";
            SaveStateManager.gameState.CompleteLevel(animator.GetInteger("level"));
            if(nbTurn <= levelConfig.nbTurnBonusObjective)
            {
                bonusText.text = "Congrats, bonus speed objective achieved!";
                SaveStateManager.gameState.CompleteObjectifLevel(animator.GetInteger("level"));
            }
            else
            {
                bonusText.text = "";
            }
            SaveStateManager.Save();
        }
        else
        {
            victoryChara.SetActive(false);
            defeatChara.SetActive(true);
            text.text = "wasted!";
            turnsText.text = "";
            bonusText.text = "";
        }
    }

    internal void SetLevelConfig(LevelConfigScriptableObject levelConfig)
    {
        this.levelConfig = levelConfig;
    }
}
