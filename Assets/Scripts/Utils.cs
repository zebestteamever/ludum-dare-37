﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utils {

    public static float TEMPO = 114f;

    public static int GetCurrentBeat(AudioSource source)
    {
        return Mathf.FloorToInt((source.time - .85f) * TEMPO / 60f);
    }

    public static bool IsNBeatsAfter(int previousBeat, int beatCount, AudioSource source)
    {
        int currentBeat = GetCurrentBeat(source);
        if (currentBeat < previousBeat)
        {
            Debug.Log("Looping music: " + currentBeat + " < " + previousBeat);
            return (previousBeat - currentBeat) % beatCount == 0;
        }
        else
        {
            return previousBeat != currentBeat && (currentBeat - previousBeat) % beatCount == 0;
        }
    }

    public static float BeatDuration()
    {
        return 60f / TEMPO;
    }

    public static void SwitchMusic(AudioSource audioSource, AudioClip music, Animator animator)
    {
        if (audioSource.clip != music)
        {
            audioSource.Stop();
            audioSource.clip = music;
            audioSource.Play();
        }
        if (animator.GetBool("mute"))
        {
            audioSource.volume = 0;
        }
    }

}
