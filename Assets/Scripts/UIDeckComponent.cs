﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIDeckComponent : MonoBehaviour {

    public enum LayoutType { STACK, HAND };
    public enum VisibilityType { HIDDEN, TOP, VISIBLE };

    public List<UICardComponent> cards = new List<UICardComponent>();
    public VisibilityType visibilityType = VisibilityType.HIDDEN;
    public LayoutType layoutType = LayoutType.STACK;

    private static Vector3 CARD_OFFSET_STACK = new Vector3(0, 0.6f, -0.5f);
    private static Vector3 CARD_OFFSET_HAND = new Vector3(6.3f, 0f, -0.5f);
    
    public int Count()
    {
        return cards.Count;
    }

    public void AddCard(UICardComponent card)
    {
        AddCard(card, -1);
    }

    public void AddCard(UICardComponent card, int position)
    {
        card.currentDeck = this;
        card.transform.SetParent(transform);
        card.targetScale = new Vector3(-1f * transform.localScale.x, transform.localScale.y, transform.localScale.z);
        if (cards.Contains(card))
        {
            cards.Remove(card);
        }

        if(position != -1 && position < cards.Count - 1)
        {
            cards.Insert(position, card);
            card.positionInDeck = position;
        }
        else
        {
            cards.Add(card);
            card.positionInDeck = cards.Count - 1;
        }
        
    }

    public void EmptyTo(UIDeckComponent deck)
    {
        foreach(UICardComponent card in cards)
        {
            deck.AddCard(card);
        }
    }

    public void DestroyAllCards()
    {
        foreach (UICardComponent card in cards)
        {
            Destroy(card.gameObject);
        }
        cards.Clear();
    }

    public void RefreshLayout()
    {
        Vector3 cardOffset = (layoutType == LayoutType.STACK) ? CARD_OFFSET_STACK : CARD_OFFSET_HAND;
        List<UICardComponent> remainingCards = new List<UICardComponent>();
        float absScale = transform.localScale.x * transform.parent.localScale.x;

        int i = 0;
        foreach (UICardComponent card in cards)
        {
            if (card.currentDeck == this)
            {
                card.frontVisible = visibilityType == VisibilityType.VISIBLE;
                card.targetPosition = transform.position + i * cardOffset * absScale;
                card.currentDeck = this;
                remainingCards.Add(card);
                card.positionInDeck = i;
                i++;
            }
        }
        cards = remainingCards;

        if (visibilityType == VisibilityType.TOP && cards.Count > 0)
        {
            cards[cards.Count - 1].frontVisible = true;
        } 
    }

}
