﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitPreviewController : MonoBehaviour {

    SpriteRenderer spriteRenderer;

    // Use this for initialization
    void Awake () {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update () {
        spriteRenderer.color = new Color(1, 1, 1, Mathf.Abs(Mathf.Sin(Time.time * 2) * 0.4f) + 0.5f);
    }
}
