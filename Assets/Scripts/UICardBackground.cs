﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICardBackground : MonoBehaviour {

    private float offsetX;
    private float offsetY;

    // Use this for initialization
    void Start () {
        offsetX = Random.Range(0f, 1f);
        offsetY = Random.Range(0f, 1f);
    }
	
	// Update is called once per frame
	void Update () {
        GetComponent<Renderer>().material.mainTextureOffset = new Vector2(Time.time * .1f + offsetX, Time.time * .02f + offsetY);
    }
}
