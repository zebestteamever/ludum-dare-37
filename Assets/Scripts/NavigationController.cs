﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationController : MonoBehaviour {

    private List<UnitController> playerUnits = new List<UnitController>();

    private List<UnitController> aiUnits = new List<UnitController>();

    public Map map;

    public GameObject unitPrefab;

    public GameObject obstaclePrefab;

    public GameObject previewPrefab;

    List<Node> playerSpawnNodeList;

    Node playerSpawnNode;

    GameObject playerSpawnPreview;

    LevelConfigScriptableObject levelConfig;

    private UIPlayerController playerController;

    private void Awake()
    {
        playerController = GameObject.Find("UIPlayerController").GetComponent<UIPlayerController>();
    }

    public void GameInit()
    {
        foreach(UnitController unit in playerUnits)
        {
            Destroy(unit.gameObject);
        }
        playerUnits.Clear();

        foreach (UnitController unit in aiUnits)
        {
            Destroy(unit.gameObject);
        }
        aiUnits.Clear();

        playerSpawnNodeList = new List<Node>();
        if (levelConfig != null)
        {
            foreach (NodeId nodeId in levelConfig.playerSpawnPoints)
            {
                Node node = map.GetNode(nodeId.i, nodeId.j);
                if (node != null)
                {
                    playerSpawnNodeList.Add(node);
                }
            }

            NodeId firstUnitNodeId = levelConfig.playerSpawnPoints[0];
            Spawn(levelConfig.firstUnit, firstUnitNodeId);

            ResetPlayerSpawnNode();
        }

    }

    internal void SetLevelConfig(LevelConfigScriptableObject levelConfig)
    {
        this.levelConfig = levelConfig;
    }

    public List<UnitController> GetUnits(bool playable)
    {
        if (playable)
        {
            return playerUnits;
        }
        else
        {
            return aiUnits;
        }
    }

    public Node GetPlayerSpawnNode()
    {
        return playerSpawnNode;
    }

    public void ResetPlayerSpawnNode()
    {
        List<Node> possibleValues = new List<Node>(playerSpawnNodeList);
        if (playerSpawnNode != null)
        {
            possibleValues.Remove(playerSpawnNode);
        }
        possibleValues.RemoveAll(item => item.hasUnit);

        if (possibleValues.Count != 0)
        {
            playerSpawnNode = possibleValues[UnityEngine.Random.Range(0, possibleValues.Count)];
            if (playerSpawnPreview != null)
            {
                Destroy(playerSpawnPreview);
            }

            if (playerController.unitCardDeck.Count() > 0)
            {
                playerSpawnPreview = Instantiate(previewPrefab, playerSpawnNode.worldPosition, Quaternion.identity) as GameObject;
                playerSpawnPreview.transform.Translate(0, 0.4f, -1);
                playerSpawnPreview.GetComponent<SpriteRenderer>().sortingOrder = playerSpawnNode.layerOrder;
            }
                
        } else
        {
            playerSpawnNode = null;
        }
    }

    internal void MoveAll(Dictionary<UnitController, List<Direction>> movements)
    {
        bool canMove = true;
        while (canMove)
        {
            canMove = false;
            foreach (KeyValuePair<UnitController, List<Direction>> entry in movements)
            {
                foreach (Direction direction in new List<Direction>(entry.Value))
                {
                    bool moved = MoveUnit(entry.Key, direction);
                    if (moved)
                    {
                        entry.Value.Remove(direction);
                        canMove = true;
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

    }

    public UnitController Spawn(Unit unit, NodeId nodeId)
    {
        Node node = map.GetNode(nodeId.i, nodeId.j);
        return Spawn(unit, node);
    }

    public UnitController Spawn(Unit unit, Node node)
    {
        if (node.hasUnit)
        {
            return null;
        }
        GameObject unitInstance;
        if (unit.obstacle)
        {
            unitInstance = Instantiate(obstaclePrefab, node.worldPosition, Quaternion.identity) as GameObject;
        }
        else
        {
            unitInstance = Instantiate(unitPrefab, node.worldPosition, Quaternion.identity) as GameObject;
        }
        
        unitInstance.transform.Translate(0, 0, -1);
        unitInstance.GetComponent<SpriteRenderer>().sprite = unit.sprite;

        UnitController unitController = unitInstance.GetComponent<UnitController>();
        unitController.node = node;
        unitController.unit = unit;
        unitController.sortingOrder = node.layerOrder;

        node.Enter(unitController);

        if (unit.playable)
        {
            playerUnits.Add(unitController);
        }
        else {
            aiUnits.Add(unitController);
        }
        
        return unitController;
    }

    public bool MoveUnit(UnitController unit, Direction direction)
    {
        int xDiff = GetXDiff(direction);
        int yDiff = GetYDiff(direction);
        Node target = map.GetNode(unit.node.gridX + xDiff, unit.node.gridY + yDiff);
        if(target != null)
        {
            if (target.hasUnit) {
                Node behindNode = map.GetNode(unit.node.gridX + xDiff * 2, unit.node.gridY + yDiff * 2);
                if (target.unit.unit.obstacle || target.unit.unit.playable == unit.unit.playable && unit.unit.name != "OrcUnit")
                {
                    return false;
                }
                else if((unit.unit.killable && target.unit.unit.killable) || behindNode == null || unit.unit.name == "OrcUnit")
                {
                    UnitController unitToKill = target.unit;
                    Vector3 targetPos = target.worldPosition + new Vector3(target.size.x * xDiff, target.size.y * yDiff);
                    unitToKill.MoveTo(targetPos, target.layerOrder);
                    KillUnit(unitToKill);
                    target.Leave();
                }
                else if(!behindNode.hasUnit)
                {
                    UnitController unitToMove = target.unit;
                    unitToMove.MoveTo(behindNode.worldPosition, behindNode.layerOrder);
                    target.Leave();
                    behindNode.Enter(unitToMove);
                }
            }
            if (!target.hasUnit) { 
                unit.MoveTo(target.worldPosition, target.layerOrder);
                unit.node.Leave();
                target.Enter(unit);
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            Vector3 targetPos = unit.node.worldPosition + new Vector3(unit.node.size.x * xDiff, unit.node.size.y * yDiff);
            unit.MoveTo(targetPos, unit.node.layerOrder);
            KillUnit(unit);
            unit.node.Leave();
            return true;
        }
    }

    void KillUnit(UnitController unit)
    {
        if (unit.unit.playable)
        {
            playerUnits.Remove(unit);
        }
        else
        {
            aiUnits.Remove(unit);
        }
        unit.Kill();
    }

    int GetXDiff(Direction direction)
    {
        if (Direction.LEFT.Equals(direction))
        {
            return -1;
        } else if (Direction.RIGHT.Equals(direction))
        {
            return 1;
        }
        return 0;
    }

    int GetYDiff(Direction direction)
    {
        if (Direction.DOWN.Equals(direction))
        {
            return -1;
        }
        else if (Direction.UP.Equals(direction))
        {
            return 1;
        }
        return 0;
    }
}
